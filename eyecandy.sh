#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Eye Candy Software                 #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Funzioni richiamate dal menu --- #

Nitrogen (){
    package="nitrogen" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   Nitrogen è già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Lxappearance (){
    package="lxappearance-gtk3" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*****************************************${Color_Off}"
        echo -e "${BYellow}***   Lxappearance è già installato   ***${Color_Off}"
        echo -e "${BYellow}*****************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Compton (){
    package="compton" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Compton è già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Polybar (){
    package="polybar" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Polybar è già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Betterlockscreen (){
    dep="i3lock"
    package="betterlockscreen" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************************${Color_Off}"
        echo -e "${BYellow}***   Betterlockscreen è già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************************${Color_Off}"
    else
        if sudo pacman -Qi $dep &> /dev/null
        then
            echo -e "${Yellow}Rimozione i3lock per gestione conflitti. Rispondere Si${Color_Off}"
            echo ""
            sudo pacman -R $dep
        fi
        trizen -S --noconfirm --noedit --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
            echo ""
            echo -e "${Yellow}Prima di utilizzare betterlockscreen eseguire il seguente comando :${Color_Off}"
            echo -e "${Yellow}betterlockscreen -u /path/to/image${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Clearine (){
    package="clearine-git" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   Clearine è già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Variety (){
    package="variety" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Variety è già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Pywal (){
    package="python-pywal" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Pywal è già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Adapta (){
    package="adapta-gtk-theme" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   Adapta è già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###########################${Color_Off}"
            echo -e "${BGreen}###   Tema installato   ###${Color_Off}"
            echo -e "${BGreen}###########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Arc (){
    package="arc-gtk-theme" 
    package2="arc-icon-theme" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**************************************${Color_Off}"
        echo -e "${BYellow}***   Arc Theme è già installato   ***${Color_Off}"
        echo -e "${BYellow}**************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###########################${Color_Off}"
            echo -e "${BGreen}###   Tema installato   ###${Color_Off}"
            echo -e "${BGreen}###########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   Arc Icon è già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package2 
        if sudo pacman -Qi $package2 &> /dev/null
        then
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   Icone installate   ###${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Paper (){
    package="paper-icon-theme" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************************${Color_Off}"
        echo -e "${BYellow}***   Paper Icon Theme è già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}#################################${Color_Off}"
            echo -e "${BGreen}###   Tema Icone installato   ###${Color_Off}"
            echo -e "${BGreen}#################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Awesome (){
    package="awesome-terminal-fonts" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***************************************************${Color_Off}"
        echo -e "${BYellow}***   Awesome Terminal Fonts è già installato   ***${Color_Off}"
        echo -e "${BYellow}***************************************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package 
        sudo fc-cache -vfs
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   Fonts installati   ###${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Jgmenu (){
    package="jgmenu"  
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   Jgmenu è già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}#############################${Color_Off}"
            echo -e "${BGreen}###   Jgmenu installato   ###${Color_Off}"
            echo -e "${BGreen}#############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo ""
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

# --- Main script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "        INSTALLAZIONE EYE CANDY"
        echo "--------------------------------------"
        echo ""
        echo "1)  Nitrogen"
        echo "2)  Lxappearance"
        echo "3)  Compton"
        echo "4)  Polybar"
        echo "5)  Jgmenu"
        echo "6)  Betterlockscreen"
        echo "7)  Clearine"
        echo "8)  Variety"
        echo "9)  Pywal"
        echo "10) Adapta (Gtk theme)"
        echo "11) Arc (Gtk theme)"
        echo "12) Paper (Icon theme)"
        echo "13) Awesome Terminal (Patched Fonts)"
        echo ""
        echo "0)  Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Nitrogen"
            Nitrogen 
            ;;
        2)
            echo "Installazione Lxappearance"
            Lxappearance
            ;;
        3)
            echo "Installazione Compton"
            Compton
            ;;
        4)
            echo "Installazione Polybar"
            Polybar
            ;;
        5)
            echo "Installazione Jgmenu"
            Jgmenu
            ;;
        6)
            echo "Installazione Betterlockscreen"
            Betterlockscreen
            ;;
        7)
            echo "Installazione Clearine"
            Clearine
            ;;
        8)
            echo "Installazione Variety"
            Variety
            ;;
        9)
            echo "Installazione Pywal"
            Pywal
            ;;
        10)
            echo "Installazione Adapta Gtk Theme"
            Adapta
            ;;
        11)
            echo "Installazione Arc Gtk Theme"
            Arc
            ;;
        12)
            echo "Installazione Paper Icons "
            Paper
            ;;
        13)
            echo "Installazione Awesome Terminal"
            Awesome
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;;
        esac
    done
