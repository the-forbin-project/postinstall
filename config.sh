#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Importazione dei vari file di configurazione     #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 
set -e
folder=$HOME"/Dev/dotfile"
gen_config_folder=".config"  
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Funzioni richiamate dai menù ---#

Import_bspwm (){
    config_folder="bspwm"
    config_folder2="sxhkd"
    config_file="bspwmrc"
    config_file2="sxhkdrc"
    package="sxhkd"
    #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"
       echo ""  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                mkdir $HOME/$gen_config_folder/$config_folder
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                mkdir $HOME/$gen_config_folder/$config_folder2
                cp $folder/$config_folder/$config_file2 $HOME/$gen_config_folder/$config_folder2/$config_file2
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATI   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                rm $HOME/$gen_config_folder/$config_folder/$config_file2
                cp $folder/$config_folder/$config_file2 $HOME/$gen_config_folder/$config_folder2/$config_file2
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATI   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Shhkd NON PRESENTE                                               !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null 
}

Import_i3 (){
   config_folder="i3"
   config_file="config"
   package="i3-wm"
   package2="i3-gaps"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"
       echo ""  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null || pacman -Qi $package2 &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                mkdir $HOME/$gen_config_folder/$config_folder
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   i3 NON PRESENTE                                               !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null  
}

Import_polybar (){
   config_folder="polybar"
   config_file="config"
   launch_file="launch.sh"
   package="polybar"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                mkdir $HOME/$gen_config_folder/$config_folder
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$launch_file $HOME/$gen_config_folder/$config_folder/$launch_file
                chmod -x $HOME/$gen_config_folder/$config_folder/$launch_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
                chmod +x $HOME/.config/polybar/launch.sh 
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$launch_file $HOME/$gen_config_folder/$config_folder/$launch_file
                chmod +x $HOME/$gen_config_folder/$config_folder/$launch_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
                chmod +x $HOME/.config/polybar/launch.sh 
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   POLYBAR NON PRESENTE                                          !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"

        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null
}

Import_dunst (){
   config_folder="dunst"
   config_file="dunstrc"
   package="dunst"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                mkdir $HOME/$gen_config_folder/$config_folder
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   DUNST NON PRESENTE                                            !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"

        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null 
}

Import_clearine (){
   config_folder="clearine"
   config_file="clearine.conf"
   package="clearine-git"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_file ]] 
            then
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   CLEARINE NON PRESENTE                                         !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"

        fi
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null 
}

Import_vim (){
   config_folder="vim"
   config_file="vimrc"
   package="vim"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$config_file ]] 
            then
                cp $folder/$config_folder/$config_file $HOME/.$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$config_file
                cp $folder/$config_folder/$config_file $HOME/.$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   VIM NON PRESENTE                                              !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"

        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null 
}

Import_zsh (){
   config_folder="zsh"
   config_file="zshrc"
   package="zsh"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$config_file ]] 
            then
                cp $folder/$config_folder/$config_file $HOME/.$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$config_file
                cp $folder/$config_folder/$config_file $HOME/.$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   ZSH NON PRESENTE                                              !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null
}

Import_neofetch (){
   config_folder="neofetch"
   config_file="config"
   package="neofetch"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                mkdir $HOME/$gen_config_folder/$config_folder
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   NEOFETCH NON PRESENTE                                         !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null  
}

Import_termite (){
   config_folder="termite"
   config_file="config"
   package="termite"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                mkdir $HOME/$gen_config_folder/$config_folder
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   TERMITE NON PRESENTE                                          !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null   
}

Import_rofi (){
   config_folder="rofi"
   config_file="config"
   package="rofi"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                mkdir $HOME/$gen_config_folder/$config_folder
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   ROFI NON PRESENTE                                             !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null 
}

Import_ranger (){
   config_folder="ranger"
   config_file="config"
   package="ranger"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   RANGER NON PRESENTE                                           !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null 
}

Import_compton (){
   config_folder="compton"
   config_file="compton.conf"
   package="compton"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se l'applicazione è installata
        if pacman -Qi $package &> /dev/null
        then 
            #Controllo se esiste già un file di configurazione
            if [[ ! -e $HOME/$gen_config_folder/$config_folder/$config_file ]] 
            then
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            else
                rm $HOME/$gen_config_folder/$config_folder/$config_file
                cp $folder/$config_folder/$config_file $HOME/$gen_config_folder/$config_file
                echo -e "${BGreen}#########################################${Color_Off}"
                echo -e "${BGreen}###   FILE CONFIGURAZIONE IMPORTATO   ###${Color_Off}"
                echo -e "${BGreen}#########################################${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   COMPTON NON PRESENTE                                          !!!${Color_Off}"
            echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null 
}

Import_Xresources (){
   config_folder="Xresources"
   config_file="Xresources"
   #Controllo esistenza repository dei file di configurazione
   if [[ ! -e $folder ]]
   then 
       echo "Sincronizzo i file di configurazione con Gitlab"
       cd $HOME/Dev
       git clone https://gitlab.com/undermod/dotfile.git
       echo ""
       echo -e "${Green}Sincronizzazzione effettuata${Color_Off}"  
   fi
   #Controllo che esista file di configurazione nel repository
   if [[ ! -e $folder/$config_folder/$config_file ]]
   then
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   FILE DI CONFIGURAZIONE NON TROVATO   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
   else
        #Controllo se esiste già un file di configurazione
        if [[ ! -e $HOME/$config_file ]] 
        then
            cp $folder/$config_folder/$config_file $HOME/.$config_file
            echo -e "${BGreen}################################${Color_Off}"
            echo -e "${BGreen}###   Xresources IMPORTATO   ###${Color_Off}"
            echo -e "${BGreen}################################${Color_Off}"
        else
            rm $HOME/$config_file
            cp $folder/$config_folder/$config_file $HOME/.$config_file
            echo -e "${BGreen}################################${Color_Off}"
            echo -e "${BGreen}###   Xresources IMPORTATO   ###${Color_Off}"
            echo -e "${BGreen}################################${Color_Off}"
        fi
   fi
   echo ""
   echo "Premere un tasto per continuare"
   read input > /dev/null
}

# --- Main script ---#

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "       IMPORTAZIONE CONFIG FILE"
        echo "--------------------------------------"
        echo ""
        echo "1)  bspwm"
        echo "2)  i3"
        echo "3)  polybar"
        echo "4)  dunst"
        echo "5)  rofi"
        echo "6)  clearine"
        echo "7)  vim"
        echo "8)  zsh"
        echo "9)  neofetch"
        echo "10) termite"
        echo "11) ranger"
        echo "12) compton"
        echo "13) Xresources"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1) 
            echo ""
            echo "Importazione file di configurazione bspwm"
            echo ""
            Import_bspwm
            ;;
        2)
            echo ""
            echo "Importazione file di configurazione i3"
            echo ""
            Import_i3
            ;;
        3)
            echo ""
            echo "Importazione file di configurazione polybar"
            echo ""
            Import_polybar
            ;;
        4)
            echo ""
            echo "Importazione file di configurazione dunst"
            echo ""
            Import_dunst
            ;;
        5)
            echo ""
            echo "Importazione file di configurazione rofi"
            echo ""
            Import_rofi 
            ;;
        6)
            echo ""
            echo "Importazione file di configurazione clearine"
            echo ""
            Import_clearine
            ;;
        7)
            echo ""
            echo "Importazione file di configurazione vim"
            echo ""
            Import_vim
            ;;
        8)
            echo ""
            echo "Importazione file di configurazione zsh"
            echo ""
            Import_zsh
            ;;
        9)
            echo ""
            echo "Importazione file di configurazione neofetch"
            echo ""
            Import_neofetch
            ;;
        10) 
            echo ""
            echo "Importazione file di configurazione termite"
            echo ""
            Import_termite
            ;;
        11)
            echo ""
            echo "Importazione file di configurazione ranger"
            echo ""
            Import_ranger
            ;;
        12)
            echo ""
            echo "Importazione file di configurazione compton"
            echo ""
            Import_compton
            ;;
        13)
            echo ""
            echo "Importazione file Xresources"
            echo ""
            Import_Xresources
            ;;
        0)
            echo ""
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;;
        esac
    done
