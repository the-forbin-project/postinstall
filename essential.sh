#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Software Essenziale                #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Funzioni richiamate dal menu --- #

Sxhkd (){
    package="sxhkd" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Sxhkd è già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Dmenu (){
    package="dmenu" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Dmenu è già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Rofi (){
    package="rofi" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************${Color_Off}"
        echo -e "${BYellow}***   Rofi è già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Dunst (){
    package="dunst" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Dunst è già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Volume_icon (){
    package="volumeicon" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}****************************************${Color_Off}"
        echo -e "${BYellow}***   Volume icon è già installato   ***${Color_Off}"
        echo -e "${BYellow}****************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Remote (){
    package="smbclient" 
    package2="gvfs-smb" 
    package3="openssh"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**************************************${Color_Off}"
        echo -e "${BYellow}***   Smbclient è già installato   ***${Color_Off}"
        echo -e "${BYellow}**************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
    fi
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   Gvfs-Smb è già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package2 
    fi 
    if sudo pacman -Qi $package3 &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   Open SSH è già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package3 
    fi 
    if sudo pacman -Qi $package &> /dev/null && sudo pacman -Qi $package2 &> /dev/null && sudo pacman -Qi $package3 &> /dev/null 
    then
        echo -e "${BGreen}###################################${Color_Off}"
        echo -e "${BGreen}###   Applicazioni installate   ###${Color_Off}"
        echo -e "${BGreen}###################################${Color_Off}"
    else
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

# --- Main script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "      INSTALLAZIONE BASE SOFTWARE"
        echo "--------------------------------------"
        echo ""
        echo "1) Sxhkd"
        echo "2) Dmenu"
        echo "3) Rofi"
        echo "4) Dunst"
        echo "5) Volume Icon"
        echo "6) Remote Folder Access"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1) 
            echo "Installazione Sxhkd"
            Sxhkd
            ;;
        2)
            echo "Installazione Dmenu"
            Dmenu
            ;;
        3)
            echo "Installazione Rofi"
            Rofi
            ;;
        4)
            echo "Installazione Dunst"
            Dunst
            ;;
        5)
            echo "Installazione Volume Icon"
            Volume_icon
            ;;
        6)
            echo "Installazione Remote Folder Access"
            Remote
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;;
        esac
    done

