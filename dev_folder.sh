#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Creazione della cartella di sviluppo             #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# La cartella predefinita creata sarà Dev/Script nella home utente

# --- Dichiarazione iniziale variabili --- # 
set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Funzioni richiamate dai menù --- #

Standard (){
    folder="Dev/Script"
    clear
    echo "######################################"
    echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
    echo "######################################"
    echo ""
    echo "--------------------------------------"
    echo "      CREAZIONE DEV FOLDER STANDARD"
    echo "--------------------------------------"
    echo ""
    echo "Procedo nella creazione della cartella dev/script nella home utente"
    echo ""
    if [[ ! -e $HOME/$folder ]]
    then
        mkdir -p $HOME/$folder
        if [[ ! -e $HOME/$folder ]]
        then
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        else
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   CARTELLA CREATA   ####${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        fi
    else
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   LA CARTELLA ESISTE GIA'   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    fi
    echo ""
    echo "Premere un tasto per continuare" 
    read input > /dev/null
}

Custom (){
    clear
    echo "######################################"
    echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
    echo "######################################"
    echo ""
    echo "--------------------------------------"
    echo "      CREAZIONE CUSTOM DEV FOLDER"
    echo "--------------------------------------"
    echo ""
    echo "Digitare nome cartella e sottocartella :"
    read folder 
    echo "Procedo nella creazione della cartella "$folder" nella home utente"
    echo ""
    if [[ ! -e $HOME/$folder ]]
    then
        mkdir -p $HOME/$folder
        if [[ ! -e $HOME/$folder ]]
        then
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        else
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   CARTELLA CREATA   ####${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        fi
    else
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   LA CARTELLA ESISTE GIA'   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    fi
    echo ""
    echo "Premere un tasto per continuare" 
    read input > /dev/null 
}

# --- Main script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "         CREAZIONE DEV FOLDER"
        echo "--------------------------------------"
        echo ""
        echo "La cartella predefinita è dev/script nella home utente"
        echo ""
        echo "1) Cartella standard"
        echo "2) Cartella personalizzata"
        echo ""
        echo "0) Menù precedente"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Creazione cartella standard"
            Standard
            ;;
        2)
            echo "Creazione cartella personalizzata"
            Custom 
            ;;
        0)
            echo "Torno al menù precedente"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;;
        esac
    done
