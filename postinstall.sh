#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Configurazione di un sistema Arch Linux          #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Funzioni richiamate dai vari menu' --- #

ConfigurazioneSistema (){
    cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "         CONFIGURAZIONE SISTEMA"
        echo "--------------------------------------"
        echo ""
        echo "1) Configurazione repository"
        echo "2) Configurazione GIT"
        echo "3) Installazione AUR helper"
        echo "4) Ottimizzazione Mirror"
        echo "5) Creazione cartella DEV"
        echo "6) Importazione file di Configurazione"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Configurazione repository AUR"
            sudo ./repository.sh 
            ;;
        2)
            echo "Configurazione account GIT"
            ./git.sh
            ;;
        3)
            echo "Installazione Trizen (AUR helper)"
            ./aur_helper.sh 
            ;;
        4)
            echo "Ottimizzazione Mirror"
            ./mirror.sh
            ;;
        5)
            echo "Creazione cartella DEV"
            ./dev_folder.sh 
            ;;
        6)
            echo "Importazione file di configurazione"
            ./config.sh 
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;;
        esac
    done
}

TuningSistema (){
    cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "             TUNING SISTEMA"
        echo "--------------------------------------"
        echo ""
        echo "1) Utilizzo CORE"
        echo "2) Configurazione Touchpad"
        echo "3) Fix Firefox Temi Scuri"
        echo "4) Installazione Vundle x Vim"
        echo "5) Importazione schema colori x Vim"
        echo "6) Installazione Oh My Zsh"
        echo "7) Installazione Powerlevel9k"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Ottimizzo utilizzo dei core"
            ./core.sh
            ;;
        2)
            echo "Configurazione Touchpad"
            ./touchpad.sh 
            ;;
        3)  
            echo "Fix Firefox testo illegibile con temi scuri" 
            ./firefox.sh
            ;;
        4)
            echo "Installazione Vundle x Vim"
            ./vundle.sh
            ;;
        5)
            echo "Importazione schema colori x Vim"
            ./molokai.sh 
            ;;
        6)
            echo "Installazione Oh My Zsh"
            ./ohmyzsh.sh
            ;;
        7)
            echo "Installazione Powerlevel9k"
            ./powerlevel9k.sh
            ;;    
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;;
        esac
    done
}

InstallazioneApplicazioni (){
    cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "       INSTALLAZIONE APPLICAZIONI"
        echo "--------------------------------------"
        echo ""
        echo "1) Essenziali"
        echo "2) File Manager"
        echo "3) Shell"
        echo "4) Ufficio"
        echo "5) Multimedia"
        echo "6) Network"
        echo "7) Utility"
        echo "8) Programmazione"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Essenziali"
            ./essential.sh 
            ;;
        2)
            echo "Installazione File Manager"
            ./FileManager.sh
            ;;
        3)
            echo "Installazione Shell e Terminale"
            ./shell.sh 
            ;;
        4)
            echo "Installazione Suite Ufficio"
            ./office.sh 
            ;;
        5)
            echo "Installazione Multimedia"
            ./multimedia.sh
            ;;
        6)
            echo "Installazione Network Utility"
            ./network.sh
            ;;
        7)
            echo "Installazione Utility"
            ./utility.sh
            ;;
        8)  
            echo "Installazione Programmazione"
            ./programmazione.sh
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;; 
        esac
    done
}

Uscita (){
    clear
    echo "######################################"
    echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
    echo "######################################"
    echo ""
    echo "--------------------------------------"
    echo "           CONFERMA USCITA"
    echo "--------------------------------------"
    echo ""
    printf "Uscire dallo script (S/N) ? "
    read input
    if [ "$input" == "S" ] || [ "$input" == "s" ]; then
        clear
        exit 0
    fi
}

Menu (){
clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "           MENU' PRINCIPALE"
echo "--------------------------------------"
echo ""
echo "1) Configurazione sistema"
echo "2) Installazione applicazioni "
echo "3) Installazione eye-candy"
echo "4) Tuning sistema"
echo ""
echo "0) Uscita"
echo ""
echo "--------------------------------------"
printf "Selezionare operazione : "
read input
case $input in
1)
    echo "Configurazione Sistema"
    ConfigurazioneSistema
    ;;
2)
    echo "Installazione Applicazioni"
    InstallazioneApplicazioni
    ;;
3)  
    echo "Installazione Eye-Candy"
    ./eyecandy.sh
    ;;
4)
    echo "Tuning Sistema"
    TuningSistema
    ;;
0)
    Uscita
    ;;
*)
    echo -e "${Red}Scelta non riconusciuta${Color_Off}"
    ;;
esac
}

# --- Parte principale dello script --- #

cod_uscita=99
while [ "$cod_uscita" -ne 0 ] 
do
    Menu
done
