#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Shell ed emulatori Terminale       #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
package="reflector"
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Controllo che reflector non sia già installato --- #

if ! $(pacman -Qi $package) &> /dev/null
then
    sudo pacman -S $package
fi 

# --- Script principale --- #
 
clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "         OTTIMIZZAZIONE MIRROR"
echo "--------------------------------------"
echo ""
echo "VERRA' GENERATA UN LISTA DEI 50 MIRROR" 
echo "PIU' VELOCI TRA ITALIA,FRANCIA E GERMANIA"
echo ""
#Backup e sostituzione della mirrorlist
echo -e "${Green} Salvataggio mirrorlist originale ...${Color_Off}"
sudo mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.orig
echo ""
echo -e "${Green} Generazione mirrorlist ottimizzata ...${Color_Off}"
echo ""
sudo reflector -c Germany -c France -c Italy -l 50 -p http --sort rate --save /etc/pacman.d/mirrorlist
echo -e "${BGreen}#################################${Color_Off}"
echo -e "${BGreen}### Mirror List ottimizzata   ###${Color_Off}"
echo -e "${BGreen}##################################${Color_Off}"
# Diamo i permessi di lettura globali (richiesti per yaourt)
sudo chmod +r /etc/pacman.d/mirrorlist
echo ""
echo "Premere un tasto per tornare al menu"
read input >/dev/null
