#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Network Utility                    #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso


# --- Funzioni richiamate dai menù --- #

Firefox (){
    package="firefox" 
    package2="firefox-i18n-it" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Firefox già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package $package2 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Thunderbird (){
    package="thunderbird" 
    package2="thunderbird-i18n-it" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**************************************${Color_Off}"
        echo -e "${BYellow}***   Thunderbird già installato   ***${Color_Off}"
        echo -e "${BYellow}**************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package $package2 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Geary (){
    package="geary" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}********************************${Color_Off}"
        echo -e "${BYellow}***   Geary già installato   ***${Color_Off}"
        echo -e "${BYellow}********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Dropbox (){
    package="dropbox" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Dropbox già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Filezilla (){
    package="filezilla" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Filezilla già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Transmission (){
    package="transmission-gtk" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***************************************${Color_Off}"
        echo -e "${BYellow}***   Transmission già installato   ***${Color_Off}"
        echo -e "${BYellow}***************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Uget (){
    package="uget" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*******************************${Color_Off}"
        echo -e "${BYellow}***   Uget già installato   ***${Color_Off}"
        echo -e "${BYellow}*******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Hexchat (){
    package="hexchat" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*******************************${Color_Off}"
        echo -e "${BYellow}***   Hexchat già installato   ***${Color_Off}"
        echo -e "${BYellow}*******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Feedreader (){
    package="feedreader" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**************************************${Color_Off}"
        echo -e "${BYellow}***   Feed Reader già installato   ***${Color_Off}"
        echo -e "${BYellow}**************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Skype (){
    package="skypeforlinux-stable-bin" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}********************************${Color_Off}"
        echo -e "${BYellow}***   Skype già installato   ***${Color_Off}"
        echo -e "${BYellow}********************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

# --- Main script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "     INSTALLAZIONE NETWORK UTILITY"
        echo "--------------------------------------"
        echo ""
        echo "1)  Firefox"
        echo "2)  Thunderbird"
        echo "3)  Geary"
        echo "4)  Dropbox"
        echo "5)  Filezilla"
        echo "6)  Transmission"
        echo "7)  Uget"
        echo "8)  Hexchat"
        echo "9)  FeedReader"
        echo "10) Skype"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Firefox "
            Firefox 
            ;;
        2)
            echo "Installazione Thunderbird"
            Thunderbird 
            ;;
        3)
            echo "Installazione Geary"
            Geary
            ;;
        4)
            echo "Installazione Dropbox"
            Dropbox
            ;;
        5)
            echo "Installazione Filezilla"
            Filezilla
            ;;
        6)
            echo "Installazione Transmission"
            Transmission
            ;;
        7)
            echo "Installazione Uget"
            Uget
            ;;
        8)
            echo "Installazione Hexchat"
            Hexchat
            ;;
        9)
            echo "Installazione Feed Reader"
            Feedreader
            ;;
        10)
            echo "Installazione Skype"
            Skype
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${$Color_Off}"
            ;;
        esac
    done

