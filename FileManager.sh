#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione File Manager                       #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso


# --- Funzioni richiamate dai menù --- #

Thunar (){
    package="thunar"
    package2="thunar-volman"
    package3="thunar-archive-plugin"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   Thunar è già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package $package2 $package3
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi   
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Nemo (){
    package="nemo" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************${Color_Off}"
        echo -e "${BYellow}***   Nemo è già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Ranger (){
    package="ranger" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   Ranger è già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

# --- Main script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "       INSTALLAZIONE FILE MANAGER"
        echo "--------------------------------------"
        echo ""
        echo "1) Thunar + Volman + Archive plugin"
        echo "2) Nemo"
        echo "3) Ranger"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Thunar"
            Thunar 
            ;;
        2)
            echo "Installazione Nemo"
            Nemo 
            ;;
        3)
            echo "Installazione Ranger"
            Ranger
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${$Color_Off}"
            ;;
        esac
    done

