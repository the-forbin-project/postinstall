#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Configurazione account GIT                       #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
BGreen='\e[1;32m'       # Verde Intenso

# --- Script --- #

clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "           CONFIGURAZIONE GIT"
echo "--------------------------------------"
echo ""
printf "Inserire nome utente GIT : "
read nome
printf "Inserire mail utente GIT : "
read mail
git config --global user.name \"$nome\"
git config --global user.email \"$mail\"
echo ""
echo -e "${BGreen}#################################################${Color_Off}"
echo -e "${BGreen}###   CONFIGURAZIONE ACCOUNT GIT EFFETTUATA   ###${Color_Off}"
echo -e "${BGreen}#################################################${Color_Off}"
echo ""
echo "Premere un tasto per tornare al menù principale"
read input >/dev/null

