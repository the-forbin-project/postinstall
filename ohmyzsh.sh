#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Oh My Zsh                          #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
package="wget"

# --- Main Script --- #
clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "        INSTALLAZIONE OH MY ZSH"
echo "--------------------------------------"
echo ""
if sudo pacman -Qi $package &> /dev/null
then
    echo -e "${Green}Installazione Oh my zsh${Color_Off}"
    sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
    echo ""
    echo -e "${BGreen}################################${Color_Off}"
    echo -e "${BGreen}###   Oh My Zsh installato   ###${Color_Off}"
    echo -e "${BGreen}################################${Color_Off}"
else
    echo -e "${Yellow}Installazione wget ... ${Color_Off}"
    echo ""
    sudo pacman -S $package
    echo -e "${Green}Installazione Oh my zsh${Color_Off}"
    echo ""
    echo -e "${Yellow}Al prompt di Oh my zsh digitare exit per continuare ${Color_Off}"
    echo ""
    sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
    echo ""
    echo -e "${BGreen}################################${Color_Off}"
    echo -e "${BGreen}###   Oh My Zsh installato   ###${Color_Off}"
    echo -e "${BGreen}################################${Color_Off}"
fi
echo "Premere un tasto per tornare al menù principale"
read input >/dev/null