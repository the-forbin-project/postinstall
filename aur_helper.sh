#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione AUR Helper (Trizen)                #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 
set -e
package=trizen
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Main script --- #

clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "       INSTALLAZIONE AUR HELPER"
echo "--------------------------------------"
echo ""
# --- Controllo che trizen non sia già installato --- #
if pacman -Qi $package &> /dev/null
then
    echo -e "${BYellow}*************************************${Color_Off}"
    echo -e "${BYellow}***   Trizen e' gia' installato   ***${Color_Off}"
    echo -e "${BYellow}*************************************${Color_Off}"
else
    git clone https://aur.archlinux.org/trizen.git
    cd trizen
    makepkg -si
    cd ..
    rm -rf trizen
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BGreen}#################################################${Color_Off}"
        echo -e "${BGreen}### Trizen e' stato installato con successo   ###${Color_Off}"
        echo -e "${BGreen}#################################################${Color_Off}"
    else
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
    fi
fi
echo ""
echo "Premere un tasto per tornare al menu"
read input >/dev/null
