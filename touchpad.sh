#!/bin/bash

#####################################################################
#                                                                   #
#   Author     :   Antonio Gistro                                   #
#   Version    :   0.1                                              #
#   Target     :   Fix per mancata configurazione Touchpad          #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Green='\e[0;32m'        # Verde
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Script --- #

clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "       FIX CONFIGURAZIONE TOUCHPAD"
echo "--------------------------------------"
echo ""
if [[ -e /etc/X11/xorg.conf.d/10-synatics.conf ]]
then
	echo -e "{$BYellow}*******************************************{$Color_Off}"
	echo -e "{$BYellow}***   FILE DI CONFIGURAZIONE PRESENTE   ***{$Color_Off}"
	echo -e "{$BYellow}*******************************************{$Color_Off}"
else
	sudo cp settings/touchpad/10-synaptics.conf /etc/X11/xorg.conf.d/10-synaptics.conf
	echo ""
	echo -e "${BGreen}############################################${Color_Off}"
	echo -e "${BGreen}###   CONFIGURAZIONE TOUCHPAD AGGIORNATA ###${Color_Off}"
	echo -e "${BGreen}############################################${Color_Off}"
fi
echo ""
echo "Premere un tasto per tornare al menù principale"
read input >/dev/null
