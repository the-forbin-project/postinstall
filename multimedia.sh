#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Multimedia                         #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso


# --- Funzioni richiamate dai menù --- #

Cmus (){
    package="cmus"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*******************************${Color_Off}"
        echo -e "${BYellow}***   Cmus già installato   ***${Color_Off}"
        echo -e "${BYellow}*******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###########################${Color_Off}"
            echo -e "${BGreen}###   Cmus installato   ###${Color_Off}"
            echo -e "${BGreen}###########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Cmus - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi    
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Deadbeef (){
    package="deadbeef"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   DeadBeef già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###############################${Color_Off}"
            echo -e "${BGreen}###   DeadBeef installato   ###${Color_Off}"
            echo -e "${BGreen}###############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   DeadBeef - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi    
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Clementine (){
    package="clementine"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   Clementine già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}#################################${Color_Off}"
            echo -e "${BGreen}###   Clementine installato   ###${Color_Off}"
            echo -e "${BGreen}#################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Clementine - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi    
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Spotify (){
    package="spotify"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Spotify già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}##############################${Color_Off}"
            echo -e "${BGreen}###   Spotify installato   ###${Color_Off}"
            echo -e "${BGreen}##############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Spotify - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi    
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Vlc (){
    package="vlc"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}******************************${Color_Off}"
        echo -e "${BYellow}***   Vlc già installato   ***${Color_Off}"
        echo -e "${BYellow}******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}##########################${Color_Off}"
            echo -e "${BGreen}###   Vlc installato   ###${Color_Off}"
            echo -e "${BGreen}##########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Vlc - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi    
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Mpv (){
    package="mpv"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}******************************${Color_Off}"
        echo -e "${BYellow}***   Mpv già installato   ***${Color_Off}"
        echo -e "${BYellow}******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}##########################${Color_Off}"
            echo -e "${BGreen}###   Mpv installato   ###${Color_Off}"
            echo -e "${BGreen}##########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Mpv - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi  
    fi  
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Vidcutter (){
    package="vidcutter"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Vidcutter già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}################################${Color_Off}"
            echo -e "${BGreen}###   Vidcutter Installato   ###${Color_Off}"
            echo -e "${BGreen}################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Vidcutter - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi    
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Ristretto (){
    package="ristretto"
    package2="tumbler"
    #Installazione ristretto
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Ristretto già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}################################${Color_Off}"
            echo -e "${BGreen}###   Ristretto installato   ###${Color_Off}"
            echo -e "${BGreen}################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Ristretto - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    #Installazione tumbler
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Tumbler già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package2
        if sudo pacman -Qi $package2 &> /dev/null 
        then
            echo -e "${BGreen}##############################${Color_Off}"
            echo -e "${BGreen}###   Tumbler installato   ###${Color_Off}"
            echo -e "${BGreen}##############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Tumbler - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Gimp (){
    package="gimp"
    package2="gimp-dbp"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*******************************${Color_Off}"
        echo -e "${BYellow}***   Gimp già installato   ***${Color_Off}"
        echo -e "${BYellow}*******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package $package2
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###########################${Color_Off}"
            echo -e "${BGreen}###   Gimp installato   ###${Color_Off}"
            echo -e "${BGreen}###########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Gimp - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Mcomix (){
    package="mcomix"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************${Color_Off}"
        echo -e "${BYellow}***   Mcomix già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}#############################${Color_Off}"
            echo -e "${BGreen}###   Mcomix installato   ###${Color_Off}"
            echo -e "${BGreen}#############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Mcomix - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

# --- Main Script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "        INSTALLAZIONE MULTIMEDIA"
        echo "--------------------------------------"
        echo ""
        echo "Sezione Audio ------------------------"
        echo "1)  Cmus"
        echo "2)  DeadBeef"
        echo "3)  Clementine"
        echo "4)  Spotify"
        echo "Sezione Video ------------------------"
        echo "5)  Vlc"
        echo "6)  Mpv"
        echo "7)  Vidcutter"
        echo "Sezione Grafica ----------------------"
        echo "8)  Ristretto"
        echo "9)  Gimp"
        echo "10) MComix"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Cmus"
            Cmus
            ;;
        2)
            echo "Installazione DeadBeef"
            Deadbeef
            ;;
        3)
            echo "Installazione Clementine"
            Clementine
            ;;
        4)
            echo "Installazione Spotify"
            Spotify
            ;;
        5)
            echo "Installazione Vlc"
            Vlc
            ;;
        6)
            echo "Installazione Mpv"
            Mpv
            ;;
        7)
            echo "Installazione Vidcutter"
            Vidcutter
            ;;
        8)
            echo "Installazione Ristretto"
            Ristretto
            ;;
        9)
            echo "Installazione Gimp"
            Gimp
            ;;
        10)
            echo "Installazione MComix"
            Mcomix
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${$Color_Off}"
            ;;
        esac
    done
