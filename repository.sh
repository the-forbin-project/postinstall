#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Configurazione repository (AUR)                  #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
db="[archlinuxfr]"
siglevel="SigLevel = Never"
server="Server = http://repo.archlinux.fr/\$arch"
Color_Off='\e[0m'
BGreen='\e[1;32m'       # Verde Intenso

# --- Script -- #

echo "" >> /etc/pacman.conf 
echo $db >> /etc/pacman.conf 
echo $siglevel >> /etc/pacman.conf 
echo $server >> /etc/pacman.conf 
sudo pacman -Syu 
clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "       CONFIGURAZIONE REPOSITORY"
echo "--------------------------------------"
echo ""
echo -e "${BGreen}######################################${Color_Off}"
echo -e "${BGreen}###   REPOSITORY AUR CONFIGURATO   ###${Color_Off}"
echo -e "${BGreen}######################################${Color_Off}"
echo ""
echo "Premere un tasto per tornare al menù principale"
read input > /dev/null
