#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione schema colori Molokai x Vim        #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
package="vim"
folder="$HOME/.vim/colors"
schema="$HOME/postinstall/settings/vim_colors/molokai.vim"

# --- Main Script --- #
clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "   INSTALLAZIONE MOLOKAI COLOR SCHEME"
echo "--------------------------------------"
echo ""
if sudo pacman -Qi $package &> /dev/null
then
    if [[ ! -e $folder ]]
    then
        echo -e "${Green}Creazione cartella colori x Vim${Color_Off}"
        mkdir -p $folder 
        echo -e "${Green}Importazione schema colori${Color_Off}"
        cp $schema $folder 
    else
        echo -e "${Green}Importazione schema colori${Color_Off}"
        cp $schema $folder 
    fi 
    echo ""
    echo -e "${BGreen}##############################${Color_Off}"
    echo -e "${BGreen}###   Molokai installato   ###${Color_Off}"
    echo -e "${BGreen}##############################${Color_Off}"
else
    echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
    echo -e "${BRed}!!!   VIM NON PRESENTE                                             !!!${Color_Off}"
    echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LO SCHEMA COLORI   !!!${Color_Off}"
    echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
fi
echo "Premere un tasto per tornare al menù principale"
read input >/dev/null
