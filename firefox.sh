#!/bin/bash

#####################################################################
#                                                                   #
#   Author     :   Antonio Gistro                                   #
#   Version    :   0.1                                              #
#   Target     :   Fix problema illeggibilità temi scuri Firefox    #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
BGreen='\e[1;32m'       # Verde Intenso

# --- Script --- #

clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "         FIX FIREFOX TEMI SCURI"
echo "--------------------------------------"
echo ""

# Apriamo e chiudiamo Firefox affinchè venga creata la cartella
sh firefox &
sleep 1
killall firefox

# Copiamo il file patch nella cartella creata
cp -r settings/firefox/chrome ~/.mozilla/firefox/*.default

echo -e "${BGreen}############################################${Color_Off}"
echo -e "${BGreen}###   IMPOSTAZIONI DI FIREFOX AGGIORNATE ###${Color_Off}"
echo -e "${BGreen}############################################${Color_Off}"
echo ""
echo "Premere un tasto per tornare al menù principale"
read input >/dev/null
