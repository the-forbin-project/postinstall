#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Vundle x Vim                       #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
package="vim"
folder="$HOME/.vim/bundle"
url="https://github.com/VundleVim/Vundle.vim.git"

# --- Main Script --- #
clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "         INSTALLAZIONE VUNDLE"
echo "--------------------------------------"
echo ""
if sudo pacman -Qi $package &> /dev/null
then
    echo -e "${Green}Installazione Vundle${Color_Off}"
    mkdir -p $folder
    cd $folder
    git clone $url
    echo ""
    echo -e "${BGreen}#############################${Color_Off}"
    echo -e "${BGreen}###   Vundle installato   ###${Color_Off}"
    echo -e "${BGreen}#############################${Color_Off}"
else
    echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
    echo -e "${BRed}!!!   VIM NON PRESENTE                                              !!!${Color_Off}"
    echo -e "${BRed}!!!   INSTALLARE IL SOFTWARE PRIMA DI IMPORTARE LA CONFIGURAZIONE   !!!${Color_Off}"
    echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
fi
echo "Premere un tasto per tornare al menù principale"
read input >/dev/null
