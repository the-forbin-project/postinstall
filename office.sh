#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Suite Ufficio                      #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Funzioni richiamate dal menu --- #

Fresh (){
    package="libreoffice-fresh" 
    package2="libreoffice-fresh-it" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************************${Color_Off}"
        echo -e "${BYellow}***   Libre Office Fresh è già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package $package2 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Still (){
    package="libreoffice-still" 
    package2="libreoffice-still-it" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************************${Color_Off}"
        echo -e "${BYellow}***   Libre Office Still è già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package $package2 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Evince (){
    package="evince" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Evince è già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

# --- Main script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "      INSTALLAZIONE SUITE UFFICIO"
        echo "--------------------------------------"
        echo ""
        echo "1) Libre Office Fresh"
        echo "2) Libre Office Still"
        echo "3) Evince"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Libre Office Fresh"
            Fresh 
            ;;
        2)
            echo "Installazione Libre Office Still"
            Still
            ;;
        3)
            echo "Installazione Evince"
            Evince
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;;
        esac
    done

