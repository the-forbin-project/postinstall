#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Software Programmazione            #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Funzioni richiamate dal menu --- #

Apache (){
    package="apache" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************${Color_Off}"
        echo -e "${BYellow}***   Apache già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
            echo ""
            printf "Avviare il server web al boot ? (S/N) : "
            read input
            if [[ $input = "s" ]] || [[ $input = "S" ]]
            then
                echo -e "${Green}Attivazione avvio Apache al boot${Color_Off}"
                sudo systemctl enable httpd
                echo -e "${Green}Attivazione effettuata${Color_Off}"
            fi
            echo ""
            echo -e "${Yellow}Ricordati di configurare httpd.conf${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Mariadb (){
    package="mysql" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Mariadb già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
            echo ""
            printf "Avviare il database al boot ? (S/N) : "
            read input
            if [[ $input = "s" ]] || [[ $input = "S" ]]
            then
                echo -e "${Green}Attivazione avvio Mariadb al boot${Color_Off}"
                sudo systemctl enable mysqld
                echo -e "${Green}Attivazione riuscita${Color_Off}"
            fi
            echo ""
            printf "Eseguire messa in sicurezza del database ? (S/N) : "
            read input
            if [[ $input = "s" ]] || [[ $input = "S" ]]
            then
                sudo mysql_secure_installation
                echo -e "${Green}Messa in sicurezza effettuata${Color_Off}"
            fi
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Php (){
    package="php"
    package2="php-apache" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}******************************${Color_Off}"
        echo -e "${BYellow}***   Php già installato   ***${Color_Off}"
        echo -e "${BYellow}******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package $package2 
        if sudo pacman -Qi $package &> /dev/null
        then
            # Modifica del file di configurazione per il funzionamento di php 
            orig='LoadModule mpm_event_module modules/mod_mpm_event.so'
            echo -e "${Green}Modifica file di configurazione Apache in corso..${Color_Off}"
            sudo sed -i '/mpm_event_module/d' ~/etc/httpd/conf/httpd.conf
            sudo echo "" >> $/etc/httpd/conf/httpd.conf
            sudo echo "# --- Php configuration --- #" >> $/etc/httpd/conf/httpd.conf
            sudo echo "#" $orig >> $/etc/httpd/conf/httpd.conf
            sudo echo "LoadModule mpm_prefork_module modules/mod_mpm_prefork.so" >> $/etc/httpd/conf/httpd.conf
            sudo echo "LoadModule php7_module modules/libphp7.so" >> $/etc/httpd/conf/httpd.conf
            sudo echo "AddHandler php7-script php" >> $/etc/httpd/conf/httpd.conf
            sudo echo "Include conf/extra/php7_module.conf" >> $/etc/httpd/conf/httpd.conf
            sudo systemctl restart httpd
            echo ""
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

PhpMyAdmin (){
    package="phpmyadmin" 
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   PhpMyAdmin già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            # Modifica del file di configurazione per il funzionamento di phpmydmin 
            echo -e "${Green}Modifica file di configurazione Php in corso..${Color_Off}"
            sudo find . -name "/etc/php/php.ini" -exec sed -i 's/;extension=mysqli/extension=mysqli/g' {} \; 
            echo -e "${Green}Creazione file di configurazione PhpMyAdmin in corso..${Color_Off}"
            sudo echo "Alias /phpmyadmin \"usr/share/webapps/phpMyAdmin\"" >> /etc/httpd/conf/extra/phpmyadmin.conf
            sudo echo "<Directory\"/usr/share/webapps/phpMyAdmin\"" >> /etc/httpd/conf/extra/phpmyadmin.conf
            sudo echo " DirectoryIndex index.php" >> /etc/httpd/conf/extra/phpmyadmin.conf
            sudo echo " AllowOverride All" >> /etc/httpd/conf/extra/phpmyadmin.conf
            sudo echo " Options FollowSymlinks" >> /etc/httpd/conf/extra/phpmyadmin.conf
            sudo echo " Require all granted" >> /etc/httpd/conf/extra/phpmyadmin.conf
            sudo echo "</Directory>" >> /etc/httpd/conf/extra/phpmyadmin.conf
            echo -e "${Green}Modifica file di configurazione Apache in corso..${Color_Off}"
            sudo sed -i '/mpm_event_module/d' /etc/httpd/conf/httpd.conf
            sudo echo "" >> $/etc/httpd/conf/httpd.conf
            sudo echo "# --- PhpMyAdmin configuration --- #" >> /etc/httpd/conf/httpd.conf
            sudo echo "Include conf/extra/phpmyadmin.conf" >> /etc/httpd/conf/httpd.conf
            sudo systemctl restart httpd
            echo ""
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

PyCharm (){
    package="pycharm-community-edition"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   PyCharm è già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Applicazione installata   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}


# --- Main script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "   INSTALLAZIONE SOFTWARE X SVILUPPO"
        echo "--------------------------------------"
        echo ""
        echo "1) Apache"
        echo "2) MariaDB"
        echo "3) Php"
        echo "4) PhpMyAdmin"
        echo "5) PyCharm Community Edition"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Apache"
            Apache 
            ;;
        2)
            echo "Installazione MariaDB"
            Mariadb
            ;;
        3)
            echo "Installazione Php"
            Php
            ;;
        4)
            echo "Installazione PhpMyAdmin"
            PhpMyAdmin
            ;;
        5)
            echo "Installazione PyCharm Community Edition"
            PyCharm
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${Color_Off}"
            ;;
        esac
    done

