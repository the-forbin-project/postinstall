#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Tema Powerlevel9k per Zsh          #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
folder="$HOME/.oh-my-zsh"
package="zsh-theme-powerlevel9k"

# --- Main Script --- #
clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "       INSTALLAZIONE POWERLEVEL9K"
echo "--------------------------------------"
echo ""
#controllo che esista la cartella di ohmyzsh
if [[ ! -e $folder ]]
then
    echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
    echo -e "${BRed}!!!   OH MY ZSH NON PRESENTE          !!!${Color_Off}"
    echo -e "${BRed}!!!   INSTALLARE PRIMA IL FRAMEWORK   !!!${Color_Off}"
    echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"    
else
    echo -e "${Green}Installazione Powerlevel9k${Color_Off}"
    sudo pacman -S --noconfirm --needed $package
    echo 'source /usr/share/zsh-theme-powerlevel9k/powerlevel9k.zsh-theme' >> ~/.zshrc
    echo ""
    if sudo pacman -Qi $package &> /dev/null
    then 
        echo -e "${BGreen}###################################${Color_Off}"
        echo -e "${BGreen}###   Powerlevel9k installato   ###${Color_Off}"
        echo -e "${BGreen}###################################${Color_Off}"
    else
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
        echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
    fi 
fi
echo "Premere un tasto per tornare al menù principale"
read input >/dev/null
