#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Utility                            #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso


# --- Funzioni richiamate dai menù --- #

Generiche (){
    package="xorg-xprop" 
    package2="xorg-xbacklight" 
    package3="unclutter" 
    package4="gucharmap" 
    package5="parcellite"
    # --- Installazione Xprop --- #
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}********************************${Color_Off}"
        echo -e "${BYellow}***   Xprop già installato   ***${Color_Off}"
        echo -e "${BYellow}*******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   Xprop installato   ###${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Xprop - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Xbacklight --- #
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   Xbacklight già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package2 
        if sudo pacman -Qi $package2 &> /dev/null 
        then
            echo -e "${BGreen}#################################${Color_Off}"
            echo -e "${BGreen}###   Xbacklight installato   ###${Color_Off}"
            echo -e "${BGreen}#################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Xbacklight - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi
    echo ""
    # --- Installazione Unclutter --- #
    if sudo pacman -Qi $package3 &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Unclutter già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package3 
        if sudo pacman -Qi $package3 &> /dev/null 
        then
            echo -e "${BGreen}################################${Color_Off}"
            echo -e "${BGreen}###   Unclutter installato   ###${Color_Off}"
            echo -e "${BGreen}################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Unclutter - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Gucharmap --- #
    if sudo pacman -Qi $package4 &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Gucharmap già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package4 
        if sudo pacman -Qi $package4 &> /dev/null 
        then
            echo -e "${BGreen}################################${Color_Off}"
            echo -e "${BGreen}###   Gucharmap installato   ###${Color_Off}"
            echo -e "${BGreen}################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Gucharmap - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Parcellite --- #
    if sudo pacman -Qi $package5 &> /dev/null
    then
        echo -e "${BYellow}*************************************${Color_Off}"
        echo -e "${BYellow}***   Parcellite già installato   ***${Color_Off}"
        echo -e "${BYellow}*************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package5 
        if sudo pacman -Qi $package5 &> /dev/null 
        then
            echo -e "${BGreen}#################################${Color_Off}"
            echo -e "${BGreen}###   Parcellite installato   ###${Color_Off}"
            echo -e "${BGreen}#################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Parcellite - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi 
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Compressione (){
    package="xarchiver" 
    package2="unzip" 
    package3="unrar" 
    # --- Installazione Xarchiver --- #
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Xarchiver già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}################################${Color_Off}"
            echo -e "${BGreen}###   Xarchiver installato   ###${Color_Off}"
            echo -e "${BGreen}################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Xarchiver - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Unzip --- #
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}********************************${Color_Off}"
        echo -e "${BYellow}***   Unzip già installato   ***${Color_Off}"
        echo -e "${BYellow}********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package2 
        if sudo pacman -Qi $package2 &> /dev/null 
        then
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   Unzip installato   ###${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Unzip - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Unrar --- #
    if sudo pacman -Qi $package3 &> /dev/null
    then
        echo -e "${BYellow}********************************${Color_Off}"
        echo -e "${BYellow}***   Unrar già installato   ***${Color_Off}"
        echo -e "${BYellow}********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package3 
        if sudo pacman -Qi $package3 &> /dev/null 
        then
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   Unrar installato   ###${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Unrar - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Dischi (){
    package="gparted" 
    package2="gnome-disk-utility" 
    # --- Installazione Gparted --- #
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Gparted già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}##############################${Color_Off}"
            echo -e "${BGreen}###   Gparted installato   ###${Color_Off}"
            echo -e "${BGreen}##############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Gparted - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Gnome Disk Utility --- #
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}********************************${Color_Off}"
        echo -e "${BYellow}***   Disks già installato   ***${Color_Off}"
        echo -e "${BYellow}********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package2 
        if sudo pacman -Qi $package2 &> /dev/null 
        then
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   Disks installato   ###${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Disks - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Masterizzazione (){
    package="xfburn"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************${Color_Off}"
        echo -e "${BYellow}***   Xfburn già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}#############################${Color_Off}"
            echo -e "${BGreen}###   Xfburn installato   ###${Color_Off}"
            echo -e "${BGreen}#############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Xfburn - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi   
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Editor (){
    package="mousepad"
    package2="vim"
    #Installazione Mousepad
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   Mousepad già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###############################${Color_Off}"
            echo -e "${BGreen}###   Mousepad installato   ###${Color_Off}"
            echo -e "${BGreen}###############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Mousepad - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi
    echo""   
    #Installazione Vim
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}******************************${Color_Off}"
        echo -e "${BYellow}***   Vim già installato   ***${Color_Off}"
        echo -e "${BYellow}******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package2
        if sudo pacman -Qi $package2 &> /dev/null 
        then
            echo -e "${BGreen}##########################${Color_Off}"
            echo -e "${BGreen}###   Vim installato   ###${Color_Off}"
            echo -e "${BGreen}##########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Vim - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi 
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Info (){
    package="neofetch" 
    package2="w3m" 
    package3="scrot"
    package4="inxi"
    # --- Installazione Neofetch --- #
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***********************************${Color_Off}"
        echo -e "${BYellow}***   Neofetch già installato   ***${Color_Off}"
        echo -e "${BYellow}***********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package 
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###############################${Color_Off}"
            echo -e "${BGreen}###   Neofetch installato   ###${Color_Off}"
            echo -e "${BGreen}###############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Neofetch - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione W3m --- #
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}******************************${Color_Off}"
        echo -e "${BYellow}***   W3m già installato   ***${Color_Off}"
        echo -e "${BYellow}******************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package2 
        if sudo pacman -Qi $package2 &> /dev/null 
        then
            echo -e "${BGreen}##########################${Color_Off}"
            echo -e "${BGreen}###   W3m installato   ###${Color_Off}"
            echo -e "${BGreen}##########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   W3m - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Scrot --- #
    if sudo pacman -Qi $package3 &> /dev/null
    then
        echo -e "${BYellow}********************************${Color_Off}"
        echo -e "${BYellow}***   Scrot già installato   ***${Color_Off}"
        echo -e "${BYellow}********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package3 
        if sudo pacman -Qi $package3 &> /dev/null 
        then
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   Scrot installato   ###${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Scrot - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Inxi --- #
    if sudo pacman -Qi $package4 &> /dev/null
    then
        echo -e "${BYellow}*******************************${Color_Off}"
        echo -e "${BYellow}***   Inxi già installato   ***${Color_Off}"
        echo -e "${BYellow}*******************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package4
        if sudo pacman -Qi $package4 &> /dev/null 
        then
            echo -e "${BGreen}###########################${Color_Off}"
            echo -e "${BGreen}###   Inxi installato   ###${Color_Off}"
            echo -e "${BGreen}###########################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Inxi - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

MonitorSistema (){
    package="stacer" 
    package2="gotop" 
    # --- Installazione Stacer --- #
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************${Color_Off}"
        echo -e "${BYellow}***   Stacer già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}#############################${Color_Off}"
            echo -e "${BGreen}###   Stacer installato   ###${Color_Off}"
            echo -e "${BGreen}#############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Stacer - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    # --- Installazione Gotop --- #
    if sudo pacman -Qi $package2 &> /dev/null
    then
        echo -e "${BYellow}********************************${Color_Off}"
        echo -e "${BYellow}***   Gotop già installato   ***${Color_Off}"
        echo -e "${BYellow}********************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package2
        if sudo pacman -Qi $package2 &> /dev/null 
        then
            echo -e "${BGreen}############################${Color_Off}"
            echo -e "${BGreen}###   Gotop installato   ###${Color_Off}"
            echo -e "${BGreen}############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Gotop - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Password (){
    package="keepassxc"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   KeepassXC già installato   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}################################${Color_Off}"
            echo -e "${BGreen}###   KeepassXC installato   ###${Color_Off}"
            echo -e "${BGreen}################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   KeepassXC - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi    
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Iso (){
    package="etcher"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************${Color_Off}"
        echo -e "${BYellow}***   Etcher già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}#############################${Color_Off}"
            echo -e "${BGreen}###   Etcher installato   ###${Color_Off}"
            echo -e "${BGreen}#############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Etcher - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi    
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Backup (){
    package="luckybackup"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}***************************************${Color_Off}"
        echo -e "${BYellow}***   Lucky Backup già installato   ***${Color_Off}"
        echo -e "${BYellow}***************************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}###################################${Color_Off}"
            echo -e "${BGreen}###   Lucky Backup installato   ###${Color_Off}"
            echo -e "${BGreen}###################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Lucky Backup - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi    
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Webcam (){
    package="cheese"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}*********************************${Color_Off}"
        echo -e "${BYellow}***   Cheese già installato   ***${Color_Off}"
        echo -e "${BYellow}*********************************${Color_Off}"
    else
        sudo pacman -S --noconfirm --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}#############################${Color_Off}"
            echo -e "${BGreen}###   Cheese installato   ###${Color_Off}"
            echo -e "${BGreen}#############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Cheese - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi    
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

Calendario (){
    package="gcalcli"
    if sudo pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Gcalcli già installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        trizen -S --noconfirm --noedit --needed $package
        if sudo pacman -Qi $package &> /dev/null 
        then
            echo -e "${BGreen}##############################${Color_Off}"
            echo -e "${BGreen}###   Gcalcli installato   ###${Color_Off}"
            echo -e "${BGreen}##############################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   Gcalcli - INSTALLAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi    
    echo "Premere un tasto per tornare al menù principale"
    read input >/dev/null
}

# --- Main Script --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "         INSTALLAZIONE UTILITY"
        echo "--------------------------------------"
        echo ""
        echo "1)  Generiche (Xprop,Xbacklight,Unclutter,Gucharmap, Parcellite)"
        echo "2)  Compressione (Xarchiver,Unzip,Unrar)"
        echo "3)  Dischi (Gparted,Gnome Disks)"
        echo "4)  Masterizzazione (Xfburn)"
        echo "5)  Editor (Mousepad,Vim)"
        echo "6)  Info (Neofetch,W3img,Inxi,Scrot)"
        echo "7)  Monitor Sistema (Stacer,GoTop)"
        echo "8)  Password (KeePassXC)"
        echo "9)  Iso (Etcher)"
        echo "10) Backup (Back in Time,Luky Backup)"
        echo "11) Webcam (Cheese)"
        echo "12) Calendario (Gcalcli)"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Utility Generiche"
            Generiche 
            ;;
        2)
            echo "Installazione Utility Compressione"
            Compressione
            ;;
        3)
            echo "Installazione Utility Dischi"
            Dischi
            ;;
        4)
            echo "Installazione Utility Masterizzazione"
            Masterizzazione
            ;;
        5)
            echo "Installazione Editor Testo"
            Editor
            ;;
        6)
            echo "Installazione Utility Info"
            Info
            ;;
        7)
            echo "Installazione Utility Monitor Sistema"
            MonitorSistema
            ;;
        8)
            echo "Installazione Utility Password"
            Password
            ;;
        9)
            echo "Installazione Utility ISO"
            Iso
            ;;
        10)
            echo "Installazione Utility Backup"
            Backup
            ;;
        11)
            echo "Installazione Utility Webcam"
            Webcam
            ;;
        12)
            echo "Installazione Utility Calendario"
            Calendario
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${$Color_Off}"
            ;;
        esac
    done
