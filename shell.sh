#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Installazione Shell ed emulatori Terminale       #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso

# --- Funzioni richiamate dai vari menu --- #

Zsh (){
    package="zsh"
    clear
    echo "######################################"
    echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
    echo "######################################"
    echo ""
    echo "--------------------------------------"
    echo "       INSTALLAZIONE ZSH SHELL"
    echo "--------------------------------------"
    echo ""
    # --- Controllo che trizen non sia già installato --- #
    if pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Zsh e' gia' installata   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        sudo pacman -S $package
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}##############################################${Color_Off}"
            echo -e "${BGreen}### Zsh e' stata installata con successo   ###${Color_Off}"
            echo -e "${BGreen}##############################################${Color_Off}"
            echo ""
            printf "Rendere Zsh la shell predefinita ? (s/n) : "
            read input
            if [[ $input = "s" ]] || [[ $input = "S" ]]
            then
                chsh -s /bin/zsh 
                echo -e "${BGreen}Zsh impostata come predefinita${Color_Off}"
            fi 
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    echo "Premere un tasto per tornare al menu"
    read input >/dev/null
}

Urxvt (){
    package="rxvt-unicode"
    clear
    echo "######################################"
    echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
    echo "######################################"
    echo ""
    echo "--------------------------------------"
    echo "          INSTALLAZIONE URXVT"
    echo "--------------------------------------"
    echo ""
    # --- Controllo che trizen non sia già installato --- #
    if pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}************************************${Color_Off}"
        echo -e "${BYellow}***   Urxvt e' gia' installata   ***${Color_Off}"
        echo -e "${BYellow}************************************${Color_Off}"
    else
        sudo pacman -S $package
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}################################################${Color_Off}"
            echo -e "${BGreen}### Urxvt e' stata installata con successo   ###${Color_Off}"
            echo -e "${BGreen}################################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    echo "Premere un tasto per tornare al menu"
    read input >/dev/null
}

Termite (){
    package="termite" 
    clear
    echo "######################################"
    echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
    echo "######################################"
    echo ""
    echo "--------------------------------------"
    echo "         INSTALLAZIONE TERMITE"
    echo "--------------------------------------"
    echo ""
    # --- Controllo che trizen non sia già installato --- #
    if pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**************************************${Color_Off}"
        echo -e "${BYellow}***   Termite e' gia' installata   ***${Color_Off}"
        echo -e "${BYellow}**************************************${Color_Off}"
    else
        sudo pacman -S $package
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}##################################################${Color_Off}"
            echo -e "${BGreen}### Termite e' stata installata con successo   ###${Color_Off}"
            echo -e "${BGreen}##################################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}!!!   OPERAZIONE NON RIUSCITA   !!!${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    echo ""
    echo "Premere un tasto per tornare al menu"
    read input >/dev/null
}

# --- Script principale --- #

cod_menu=99
    while [ "$cod_menu" -ne 0 ]
    do
        clear
        echo "######################################"
        echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
        echo "######################################"
        echo ""
        echo "--------------------------------------"
        echo "           INSTALLAZIONE SHELL"
        echo "--------------------------------------"
        echo ""
        echo "1) Zsh"
        echo "2) Urxvt"
        echo "3) Termite"
        echo ""
        echo "0) Menù principale"
        echo ""
        echo "--------------------------------------"
        printf "Selezionare operazione : "
        read cod_menu
        case $cod_menu in
        1)
            echo "Installazione Zsh Shell"
            Zsh  
            ;;
        2)
            echo "Installazione Urxvt"
            Urxvt 
            ;;
        3)
            echo "Installazione Termite"
            Termite 
            ;;
        0)
            echo "Torno al menù principale"
            ;;
        *)
            echo -e "${Red}Scelta non riconusciuta${$Color_Off}"
            ;;
        esac
    done


