#!/bin/bash
#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Utilizzo di tutti i core disponibili             #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione Variabili ---#

set -e
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
BGreen='\e[1;32m'       # Verde Intenso
Color_Off='\e[0m'

# --- Script --- #

clear
echo "######################################"
echo "#     POSTINSTALL SCRIPT Ver. 0.1    #"
echo "######################################"
echo ""
echo "--------------------------------------"
echo "          OTTIMIZZAZIONE CORE"
echo "--------------------------------------"
echo ""

numberofcores=$(grep -c ^processor /proc/cpuinfo)
case $numberofcores in
    16)
        echo -e "${Green}Hai" $numberofcores" core.${Color_Off}"
        echo -e "${Green}"Modifico la makeflags per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j17"/g' /etc/makepkg.conf
        echo "Modifico le impostazioni di compressione per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 16 -z -)/g' /etc/makepkg.conf
        ;;
    8)
        echo -e "${Green}Hai" $numberofcores" core.${Color_Off}"
        echo -e "${Green}Modifico la makeflags per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j9"/g' /etc/makepkg.conf
        echo -e "${Green}Modifico le impostazioni di compressione per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 8 -z -)/g' /etc/makepkg.conf
        ;;
    6)
        echo -e "${Green}Hai" $numberofcores" core.${Color_Off}"
        echo -e "${Green}Modifico la makeflags per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j7"/g' /etc/makepkg.conf
        echo -e "${Green}Modifico le impostazioni di compressione per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 6 -z -)/g' /etc/makepkg.conf
        ;;       
    4)
        echo -e "${Green}Hai" $numberofcores" core.${Color_Off}"
        echo -e "${Green}Modifico la makeflags per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j5"/g' /etc/makepkg.conf
        echo -e "${Green}Modifico le impostazioni di compressione per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 4 -z -)/g' /etc/makepkg.conf
        ;;
    2)
        echo -e "${Green}Hai" $numberofcores" core.${Color_Off}"
        echo -e "${Green}Modifico la makeflags per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j3"/g' /etc/makepkg.conf
        echo -e "${Green}Modifico le impostazioni di compressione per "$numberofcores" core.${Color_Off}"
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 2 -z -)/g' /etc/makepkg.conf
        ;;
    *)
        echo -e "${Red}Non riesco a stabilire quanti core sono disponibili.${Color_Off}"
        echo -e "${Red}Procedere manualmente.${Color_Off}"
        ;;
esac
echo ""
echo -e "${BGreen}#################################################################################${Color_Off}"
echo -e "${BGreen}###  Tutti i core saranno utilizzati durante la compilazione e compressione  ####${Color_Off}"
echo -e "${BGreen}#################################################################################${Color_Off}"
echo ""
echo "Premere un tasto per tornare al menu"
read input >/dev/null